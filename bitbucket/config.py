import os
import stat

try:
    from configparser import SafeConfigParser, NoSectionError, NoOptionError
except ImportError:
    from ConfigParser import SafeConfigParser, NoSectionError, NoOptionError

def get_default(config, section, key, default=''):
    try:
        return config.get(section, key)
    except NoSectionError:
        return default
    except NoOptionError:
        return default


CONFIG_FILE = os.path.expanduser('~/.bitbucket')
config = SafeConfigParser()
config.read([CONFIG_FILE])

USERNAME = get_default(config, 'auth', 'username')
PASSWORD = get_default(config, 'auth', 'password', None)
SCM = get_default(config, 'options', 'scm', 'hg')
PROTOCOL = get_default(config, 'options', 'protocol', 'https')

if PASSWORD and (os.stat(CONFIG_FILE).st_mode & stat.S_IROTH):
    print ('****************************************************\n'
           '  Warning: config file is readable by other users.\n'
           '  If you are storing your password in this file,\n'
           '  it may not be secure. Try running: chmod 750 ~/.bitbucket\n'
           '****************************************************')
